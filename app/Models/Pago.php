<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pago extends Model
{
    protected $fillable = [
        'usuario', 'tipo_pago', 'fecha', 'descripcion', 'updated_at', 'created_at'
    ];
}
