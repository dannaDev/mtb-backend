<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Pago;


class PagoController extends Controller
{
   

    public function pagos(){
        $pagos = Pago::join('users', 'pagos.usuario', '=', 'users.id')
        ->select('pagos.*', 'users.nombres', 'users.apellidos')
        ->where('estado', '=', 1)
        ->orderBy('created_at', 'desc')
        ->get();
        return response()->json([
            'status' => true,
            'data' => $pagos
        ], 200);
    }

    public function filterPagos($documento){
        $pagos = Pago::join('users', 'pagos.usuario', '=', 'users.id')
        ->select('pagos.*', 'users.nombres', 'users.apellidos')
        ->where('users.documento', '=', $documento)
        ->orderBy('created_at', 'desc')
        ->get();
        return response()->json([
            'status' => true,
            'data' => $pagos
        ], 200);
    }
    public function pagosUsuario($id){
        $pagos = Pago::select('*')
        ->where('usuario', '=', $id)
        ->orderBy('created_at', 'desc')

        ->get();
        return response()->json([
            'status' => true,
            'data' => $pagos
        ],200);
    }

    function desactivar($id){
        $pagos = Pago::find($id);
    
           $pagos->estado = 0;
        $pagos->save();
    }   


    public function createPagos(Request $request){
        $created = new Pago();

        $created -> usuario = $request->usuario;
        $created -> tipo_pago = $request->tipo_pago;
        $created -> fecha = $request->fecha;
        $created -> descripcion = $request->descripcion;

        $created->save();
        try{
            return response()->json([
                'status' => true,
                'httpCode' => 200,
                'mensaje' => 'Pago creado con éxito',
                'error' => null,
            ], 200);
        }catch(\Exception $e){
            return response()->json([
                'status' => false,
                'httpCode' => 500,
                'mensaje' => 'Error al crear el pago',
                'error' => $e->getMessage(),
            ], 500);
        }
    
    }
}
