<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller
{


    public function listUser(){

        $users = User::all();
        return response()->json([
            'status' => true,
            'data' => $users
        ], 200);
    }


    public function Register(Request $request){


		$validation = $this->ValidateInputRegister($request->documento, $request->documento);
		if($validation){
			return response()->json($validation->original, 200);
		}

        $existUser = $this->ValidateExistUser($request->documento);
        if($existUser){
            return response()->json($existUser->original, 200);
        }

		$register = $this->NewUser($request);
		return response()->json($register->original);


    }
   
    public function NewUser(Request $request){
        $created = new User();

        $created -> nombres = $request->nombres;
        $created -> apellidos = $request->apellidos;
        $created -> documento = $request->documento;
        $created -> telefono = $request->telefono;
        $created -> email = $request->email;
        $created -> rol = 2;
        $created -> usuario = $request->documento;
        $created->  password = bcrypt($request->documento);

        $created->save();
        try{
            return response()->json([
                'status' => true,
                'httpCode' => 200,
                'mensaje' => 'Usuario creado con éxito',
                'error' => null,
            ], 200);
        }catch(\Exception $e){
            return response()->json([
                'status' => false,
                'httpCode' => 500,
                'mensaje' => 'Error al crear el usuario',
                'error' => $e->getMessage(),
            ], 500);
        }
    
    }
    
    public function ValidateInputRegister($usuario, $password){
    
        
        $input = array('usuario' => $usuario, 'password' => $password);
        $validar = \Validator::make($input,[
            'usuario' => 'required',
            'password' => 'required',
        ]);
        
        // Enviamos la respuesta de validacion en caso de ser incorrecta
        if($validar->fails()){
            return response()->json([
                'mensaje' => 'Validacion de campos',
                'error' => $validar->messages(),
            ], 200);
        }
    }

    public function ValidateExistUser($usuario){

        $input = array('usuario' => $usuario);
        $validar = \Validator::make($input,[
            'usuario' => 'required|unique:users',
        ]);

        if($validar->fails()){
            return response()->json([
                'mensaje' => 'Usuario Existente',
                'error' => $validar->messages(),
            ], 200);
        }
    }

}
