<?php

namespace App\Http\Controllers;

use Mail;
use App\User;
use App\Aspirantes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

class LoginController extends Controller
{
    public  function  login(Request  $request) {
		$input = $request->only('usuario', 'password');
		$jwt_token = null;
		if (!$jwt_token = JWTAuth::attempt($input)) {
			return  response()->json([
				'status' => 'bad',
				'message' => 'Las credenciales no son validas.',
			], 401);
		}

		$usuario =  User::select('*')->where('usuario', '=', $request->usuario)->get();

		return  response()->json([
			'status' => 'ok',
			'token' => $jwt_token,
			'usuario' => $usuario
		]);
	}



    public function register(Request $request)
        {
                $validator = Validator::make($request->all(), [
                'usuario' => 'required|string|max:100',
                'email' => 'required|string|email',
                'password' => 'required|string|min:6|confirmed',
            ]);

            if($validator->fails()){
                    return response()->json($validator->errors()->toJson(), 400);
            }

            $user = User::create([
                'name' => $request->get('name'),
                'email' => $request->get('email'),
                'password' => Hash::make($request->get('password')),
            ]);

            $token = JWTAuth::fromUser($user);

            return response()->json(compact('user','token'),201);
        }


    public function nuevoUsuario(Request $request)
{       
    $validar = User::select('*')
        ->where('documento', '=', $request->documento)
        ->get();
        if(count($validar)>0){
            return response()->json([
                'status' => true,
                'mensaje' => 'El documento ya existe.'
            ], 200);
        }else{
       $usuario = new User();
        $usuario->nombres = $request->nombres;
        $usuario->apellidos = $request->apellidos;
        $usuario->email = $request->email;
        $usuario->documento = $request->documento;
        $usuario->tipoDocumento = $request->tipoDocumento;
        $usuario->usuario = $request->usuario;
        $usuario->password = bcrypt($request->password);
        $usuario->rol = $request->rol;
        $usuario->save();
       try{
        return response()->json([
            'status' => true,
            'mensaje' => '¡Usuario creado con exito!',
        ], 200);

       }catch(\Exception $e){
           return response()->json([
            'staus' => false,
            'mensaje' => '¡Error al registrar, por favor intentelo de nuevo'
           ], 500);
       }
    }
 
        
}

    public function nuevoAspirante(Request $request){
        // validamos si existe

        $validar = Aspirantes::select('*')
        ->where('numerodocumento', '=', $request->documento)
        ->get();

        if(count($validar)>0){
            return response()->json([
                'status' => true,
                'mensaje' => 'El documento ya existe.'
            ], 200);
        }else{
             // $snappyamigo = $request->all(); 
        $nuevo = new Aspirantes();
        $nuevo->nombres = $request->nombres;
        $nuevo->apellidos = $request->apellidos;
        $nuevo->email = $request->email;
        $nuevo->tipodocumento = $request->tipoDocumento;
        $nuevo->numerodocumento = $request->documento;
        $nuevo->telefono = $request->telefono;
        $nuevo->direccion = $request->direccion;
        $nuevo->save();
        try{
            // Mail::send('email.registro', ['nombres' => $request->nombres], function($msj) use ($request){
            //     $email = $request->email;
            //     $msj->from('orozcojean81@gmail.com', 'Jean Orozco');
            //     $msj->to($email, $request->nombres);
            //     $msj->subject('¡Notificación!');
            // });
            return response()->json([
                'status' => true,
                'mensaje' => 'Registro éxitoso'
            ], 200);
        }catch(\Excepction $e){
            return response()->json([
                'status' => false,
                'mensaje' => $e->getMessage()
            ], 500);
        }
        }

       
        // return $request;

    }

    public function validar_usuario($usuario){
        $get_usuario = User::select('usuario')
        ->where('usuario', '=', $usuario)
        ->get();
        return response()->json([
            'status' => true,
            'mensaje' => count($get_usuario)
        ],200);
    }


}
