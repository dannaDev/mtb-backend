<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use JWTAuth;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Tymon\JWTAuth\Exceptions\JWTException; 

class SesionController extends Controller
{
    public function login(Request $request){


		$validation = $this->ValidateInputLogin($request->usuario, $request->password);
		if($validation){
			return response()->json([$validation->original], 400);
		}

		$login = $this->ValidarCredenciales($request);
		return response()->json($login->original);

    }

	private function ValidateInputLogin($usuario, $password){

		$input = array('usuario' => $usuario, 'password' => $password);
		$validar = \Validator::make($input,[
            'usuario' => 'required',
            'password' => 'required',
        ]);
        
        // Enviamos la respuesta de validacion en caso de ser incorrecta
        if($validar->fails()){
            return response()->json([
                'mensaje' => 'Validacion de campos',
                'error' => $validar->messages(),
            ], 401);
		}
	}

	private function ValidarCredenciales($request){

		
        $input = $request->only('usuario', 'password');
        $jwt_token = null;
        

		if (!$jwt_token = JWTAuth::attempt($input)) {
			return response()->json([
				'status' => false,
                'httpCode' => 401,
				'mensaje' => 'Login incorrecto',
                'error' => 'Las credenciales no son validas'
			], 401);
		}

		$usuario =  User::select('*')->where('usuario', '=', $request->usuario)->get();

		return response()->json([
			'status' => true,
            'mensaje' => 'Login correcto',
            'httpCode' => 200,
			'token' => $jwt_token,
			'usuario' => $usuario
        ], 200);
		
	}


	
}
