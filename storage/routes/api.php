<?php

// api de login -no necesita token-
Route::group(['middleware' => 'cors'], function () {
    Route::post('login', 'LoginController@login');
    Route::post('/nuevoAspirante', 'LoginController@nuevoAspirante');
    Route::post('/nuevoUsuario', 'LoginController@nuevoUsuario');
    Route::get('/validarUsuario/{usuario}', 'LoginController@validar_usuario');
    Route::get('/mypets/{usuario}', 'MascotasController@mypets');
    Route::get('/myservicios/{usuario}', 'ReservasController@misreservas');
    Route::put('/mypetsupdate/{id}', 'MascotasController@mypetsupdate');
    Route::put('/deletedmypets/{id}', 'MascotasController@deletedmypets');
    Route::get('/prestadores', 'PrestadoresController@prestadores');
    Route::get('/aspirantes', 'PrestadoresController@aspirantes');
    Route::get('/snappyamigos', 'PrestadoresController@prestadores');
    Route::post('/reservar', 'ReservasController@reservar');
    Route::post('/newpets', 'MascotasController@newpets');
});
Route::group(['middleware' => ['cors', 'jwt.auth']], function () {
   
    // cerrar sesion 
    Route::post('/logout', 'AuthController@logout');
});

