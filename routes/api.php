<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/login', 'App\Http\Controllers\SesionController@login');
Route::post('/register', 'App\Http\Controllers\UserController@Register');

//PAGOS
Route::get('/pago', 'App\Http\Controllers\PagoController@pagos');
Route::get('/pagos/filter/{documento}', 'App\Http\Controllers\PagoController@filterPagos');
Route::post('/pago/new', 'App\Http\Controllers\PagoController@createPagos');
Route::put('/pago/desactivar/{id}', 'App\Http\Controllers\PagoController@desactivar');
Route::get('/pago/usuario/{id}', 'App\Http\Controllers\PagoController@pagosUsuario');


//USUARIOS
Route::get('/usuarios', 'App\Http\Controllers\UserController@listUser');

Route::group(['middleware' => 'jwt.auth'], function () {

});