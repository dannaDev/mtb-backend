<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    Hola {{!!$nombres!!}}, <br>

    ¡Gracias por tu interés en formar parte de la familia Snappy!
    En breve nos pondremos en contacto contigo para informarte cual será el proceso a seguir. <br> <br>

    Saludos.

</body>
</html>